

<!--Modal for sign up-->
     <!-- Modal content-->
<div class="modal fade" id="search" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        
    <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h3><i class="fa fa-user"></i> Search</h3>
    </div>
        
    <div class="modal-body" style="padding:40px 50px;">
    <form role="form" name="form" method="POST" id="contactform" action="#">
           
           <div class="md-form">
                <i class="fa fa-phone prefix" aria-hidden="true"></i>
                <input type="number" id="smobile" name="smobile" class="form-control" required >
                <label for="smobile">Contact No</label>
            </div>
            
            <div class="md-form">
                <i class="fa fa-location-arrow prefix" aria-hidden="true"></i>
                <input type="text" id="slocat" name="slocat" class="form-control " required>
                <label for="slocat"> Location</label>
            </div>
            
            
            <!-- Select Class with Subject -->
            
            
            
            
            <?php
            //Include database configuration file
            include('connection.php');
             //Get all country data
            $query = $conn->query("SELECT * FROM class WHERE status = 1 ORDER BY class_name ASC");
            //Count total number of rows
            $rowCount = $query->num_rows;
            ?>

            <div class="md-form">
            
             <select required="" name="sclasses" id="sclasses" class="form-control dropdown-toggle-split">
              
               <option value="">Select Class</option>
                <?php
                if($rowCount > 0)
                {
                    while($row = $query->fetch_assoc())
                    { 
                        echo '<option value="'.$row['class_id'].'">'.$row['class_name'].'</option>';
                    }
                }
                else
                {
                    echo '<option value="">Class not available</option>';
                }  
                ?>
             </select>
             
            </div>
            
            

        <div class="md-form">
            
            <div id="ssubject" class="form-control" ></div>
        
        </div>
        <div class="md-form">
            <button id="ssubmit" type="submit" name="ssubmit" value="send" class="btn btn-info btn-block">
               Search Teacher</button>
        </div>

    
            
    </form>
    </div>

   

   </div>
   </div>
   </div>
   

<script src="js/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function()
{
    $('#sclasses').on('change',function()
    {
        var classID = $(this).val();
        if(classID)
        {
            $.ajax
            ({
                type:'POST',
                url :'getclassdata.php',
                data:'class_id='+classID,
                success:function(html)
                {
                    //alert('tested ok');
                   $('#ssubject').html(html);
                  
                }
            }); 
        }
        else
        {
            $('#ssubject').html('<option value="">Select Class first</option>');
            //$('#city').html('<option value="">Select state first</option>'); 
        }
    });
           
    $('#ssubmit').click(function()
    {
     //alert('data onclick inserted successfully');

       var insert = [];
     //Get values of the input fields and store it into the variables.
        var smobile  = $("#smobile").val();
        var slocat   = $("#slocat").val();
        var sclasses = $("#sclasses").val();
        var ssubmit  = $("#ssubmit").val();
        
//alert(slocat);
        
     $('.get_value').each(function()
     {
         if($(this).is(":checked"))
         {
           insert.push($(this).val());
         }
     });

    // insert = insert.toString();

     $.ajax
     ({
        url: "seinsert.php",
        method: "POST",
        data  : {insert:insert,smobile:smobile,slocat:slocat,sclasses:sclasses,
            ssubmit:ssubmit},
        success: function(data)
        {
          //alert(data);
           if(data == 11) 
           {
              alert('Good Job Enquiry Submitted successfully');
             
            }
            else if(data == 0)
            {
               alert('Please Enter Correct details'); 
            }
        }
     });
    });


});






</script>

     
     
     