
<!--Modal for sign up-->
     <!-- Modal content-->
<div class="modal fade" id="tsignupmodal" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
        
    <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h3><i class="fa fa-user"></i> Registeration</h3>
    </div>
        
    <div class="modal-body" style="padding:40px 50px;">
        <form role="form" name="form" method="POST" id="contactform" action="#">
            
            <div class="md-form">
                <i class="fa fa-user prefix"></i>
                <input type="text" id="t_name"  class="form-control " name="t_name" required 
                pattern="^[A-Za-z]+" title="Please enter character only" >
                <label for="t_name"> Enter Full Name</label>
            </div>
            
            <div class="md-form">
                <i class="fa fa-user prefix"></i>
                <input type="text" id="t_username" class="form-control" name="t_username"  required>
                <label for="t_username">Enter Username</label>
            </div>




            <div class="md-form">
                <i class="fa fa-lock prefix"></i>
                <input type="password" id="t_password" class="form-control" name="t_password"  minlength="6" title="Must contain at least 6 or more characters" required >
                <label for="t_password">Enter Password</label>
            </div>
            <div class="md-form">
              <i class="fa fa-lock prefix"></i>
               <input type="password" id="cpassword" class="form-control" name="cpassword"  required >
                <label for="cpassword">Confirm Password</label>
            </div>

              
                            
                

            <div class="md-form">
                <i class="fa fa-envelope prefix"></i>
                <input type="email" id="t_email" class="form-control" name="t_email" required>
                <label for="t_email">Enter Email</label>
            </div>
            <div class="md-form">
                <i class="fa fa-envelope prefix"></i>
                <input type="text" id="t_contactno" class="form-control" name="t_contactno"  required>
                <label for="t_contactno">Enter Contact</label>
            </div>

            
            <div class="md-form">
                <i class="fa fa-envelope prefix"></i>
                <input type="number" id="t_experience" class="form-control"  name="t_experience"  required>
                <label for="t_experience">Enter Teaching Experience</label>
            </div>
            <div class="md-form">
                <i class="fa fa-home prefix" aria-hidden="true"></i>
                <input type="text" id="t_location"  class="form-control" name="t_location" required>
                <label for="t_location">Enter Address</label>
            </div>
 
 <!-- Select Class with Subject -->
         
            <?php
            //Include database configuration file
            include('connection.php');
             //Get all country data
            $query = $conn->query("SELECT * FROM class WHERE status = 1 ORDER BY class_name ASC");
            //Count total number of rows
            $rowCount = $query->num_rows;
            ?>

            <div class="md-form">
            
             <select required name="t_qualification" id="t_qualification" class="form-control dropdown-toggle-split">
              
               <option value="">Select Qualification</option>
                <?php
                if($rowCount > 0)
                {
                    while($row = $query->fetch_assoc())
                    { 
                        echo '<option value="'.$row['class_id'].'">'.$row['class_name'].'</option>';
                    }
                }
                else
                {
                    echo '<option value="">Qualification not available</option>';
                }  
                ?>
             </select>
             
            </div>
            
            

        <div class="md-form">
            
            <div id="t_subject" class="form-control"> </div>
        
        </div>

           <div class="md-form">
            <button type="submit" value="Send" class="btn btn-info btn-block" name="tsubmit" id="tsubmit" >
               Sign up</button>
          </div>

        </form>
    </div>

   </div>
   </div>
   </div>
    <script  src="js/jquery.min.js"></script>
   <script type="text/javascript"> 
$(document).ready(function()
{  
   $('#t_qualification').on('change',function()
    {
        var classID = $(this).val();
        if(classID)
        {
            $.ajax
            ({
                type:'POST',
                url :'getclassdata.php',
                data:'class_id='+classID,
                success:function(html)
                {
                    //alert('tested ok');
                   $('#t_subject').html(html);
                  
                }
            }); 
        }
        else
        {
            $('#t_subject').html('<option value="">Select Class first</option>');
            //$('#city').html('<option value="">Select state first</option>'); 
        }
    });







  //alert('data ready inserted successfully');
    $('#tsubmit').click(function()
    {
       // console.log("button clicked");
      // alert('data onclick inserted successfully');
       var insert = [];
     //Get values of the input fields and store it into the variables.
        
        var t_name          = $("#t_name").val();
        var t_username      = $("#tsignupmodal #t_username").val();
        var t_password      = $("#tsignupmodal #t_password").val();
        var cpassword       = $("#tsignupmodal #cpassword").val();
        var t_email         = $("#t_email").val();
        var t_contactno     = $("#t_contactno").val();
        var t_location      = $("#t_location").val();
        var t_qualification = $("#t_qualification").val();
        var t_experience    = $("#t_experience").val();
        var tsubmit         = $("#tsubmit").val();
      //alert(t_username);
     
    if (t_password != cpassword) 
    {
            alert("Passwords do not match.");
            return false;
    }
    else
    {
      
        $('.get_value').each(function()
        {
         if($(this).is(":checked"))
          {
            insert.push($(this).val());
          }
        });

       //alert(fname);
      
        $.ajax
        ({
            url   : "tsignupinsert.php",
            method: "POST",
            data  : {insert:insert,t_name:t_name,t_username:t_username,t_password:t_password, 
                     t_email:t_email,t_contactno:t_contactno,t_location:t_location,
                     t_qualification:t_qualification,t_experience:t_experience,tsubmit:tsubmit},
            success:function(data)
            {
               // alert(data);
              if(data == 0) 
              {
                
                alert('Please fill all required fields');
                return false;
              }
              else if(data == 1)
              {
                   
                  alert('good job data inserted successfully');
              }
              else if(data == 2)
              {

                alert('User already exits');
                return false;
              }
            }
         });

     }
 });

});


</script>