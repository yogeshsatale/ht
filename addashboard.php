<!DOCTYPE html>
<html lang="en">
<head>
  <title>ADMIN</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <link href="css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/compiled.min.css">
    <link rel="stylesheet" type="text/css" href="css/astyle.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
  #wrapper{width: 100%;}
    /* Remove the navbar's default margin-bottom and rounded borders */
    .navbar 
    {
      margin-bottom: 0;
      border-radius: 0;
    }
    .sidenav ul li{border:1px solid red;}
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
     /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) 
    {
      .sidenav 
      {
        height: auto;
        margin: 0;
        padding-right:0px;
        padding-top: 20px;
       background-color: rgba(255,255,255,.15);
       
      }
    }
    .sidenav a 
    {
    padding: 8px 8px 8px 2px;
    text-decoration: none;
    font-size: 20px;
    color: #818181;
    display: block;
    transition: 0.3s;
   }

.sidenav a:hover, .offcanvas a:focus
{
    color: #f1f1f1;
}
    
    /* Set black background color, white text and some padding */
    footer 
    {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
 .nav-link{font-weight:bold;}  
      
h1{font-weight: bolder; }
    
  </style>
</head>
<body class="view">
<div id="wrapper">
<nav class="navbar navbar-dark danger-color-dark navbar-fixed-top">
  
    <div class="navbar-header" style="font-weight:bold;">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <i class="fa fa-bars"></i></button>
      <a class="navbar-brand" href="#">HOMETutor</a>
    </div>
    
    <div class="container">
        <div class="collapse navbar-collapse" id="myNavbar">
      
           <ul class="navbar-nav">
              
              <li class="nav-item">
                  <a class="nav-link" data-toggle="modal" data-target="#">About Us</a>
              </li>
              
              <li class="nav-item">
                  <a class="nav-link" data-toggle="modal" data-target="#slmodal">Feedback</a>
              </li>
              
              <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#tlmodal">Teacher</a>
              </li>
              
              <li class="nav-item">
               <a class="nav-link" data-toggle="modal" data-target="#adlmodal">Blog</a>
              </li>
            </ul>
            <!--Navbar icons-->
            <ul class="navbar-nav nav-flex-icons" >
            
              <li class="nav-item ">
                <a class="nav-link" href="#">1<i class="fa fa-bell"></i></a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="#">1<i class="fa fa-comment"></i></a>
              </li>  
              <li class="nav-item ">
                <a class="nav-link" href="#">1 <i class="fa fa-envelope"></i></a>
              </li>
              <li class="dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user"></i></a>
                <ul class="dropdown-menu dropdown-user">
                  <li><a href="#"><i class="fa fa-user"></i> User Profile</a></li>
                  <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                  <li class="divider"></li>
                  <li><a href="index.php"><i class="fa fa-sign-out"></i>Logout</a></li>
                </ul>
              </li>
            </ul>
        </div>
    </div>
</nav>
 <!--End of navbar-->

 <!--Main body Containt -->

<div class="container-fluid text-center" style="margin-top:66px; padding-right:0;">
  <div class="row">
    <div class="col-sm-2 " style="border:1px solid black; display:list-item;">
    <div class="sidenav">
    <ul class="navbar-nav ">
      
      <li class="nav-item"><a class="nav-link" href="#">Dashboard</a></li>
      <br>
      <li class="nav-item"><a class="nav-link" href="#">Shedule</a></li><br>
      <li class="nav-item"><a class="nav-link" href="#">Payment</a></li><br>
    </ul>
    </div>
    </div>
    
    <div class="col-sm-7 text-left" style="margin:0;">
        <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header" style="border:0; ">Dashboard</h1>
          <!-- /.col-lg-12 -->
        </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-sm-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    
                                    <i class="fa fa-bullhorn fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">26</div>
                                    <div>No of Enquiries!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer danger-color-dark">
                                <span class="pull-left" style="color:white;">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right" style="color:white;"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
            </div>
            
            
             <div class="col-sm-4 col-sm-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                  <i class="fa fa-bullhorn fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">26</div>
                                    <div>No of Enquiries!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer danger-color-dark">
                                <span class="pull-left" style="color:white;">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right" style="color:white;"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
            </div>
            

            <div class="col-sm-4 col-sm-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    
                                    <i class="fa fa-bullhorn fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">26</div>
                                    <div>No of Enquiries!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer danger-color-dark">
                                <span class="pull-left" style="color:white;">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right" style="color:white;"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
            </div>
            </div>
    
     <hr>
     <div class="row"> 
      <h1>Welcome</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
     </div>
       
      <hr>
      <div class="row">
      <h3>List Of Enquries</h3>
      <table class="table">
      <tr>
      <th>Sr.No.</th>
          <th>Date&Time</th>
          <th>Contact No</th>
          <th>Subject</th>
          <th>Location</th>
      </tr>
      <tr>
         <td>1</td>
          <td>29Oct2016|10:10AM</td>
          <td>123456</td>
          <td>Math</td>
          <td>Pune</td>
      </tr>

      </table>
     </div>
    </div>

  <div class="col-md-3">
   <div class="row">  
    <div class="chat-panel panel panel-default">
        <div class="panel-heading">
          <i class="fa fa-comments fa-fw"></i> Chat
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <ul class="chat">
            <li class="left clearfix">
              <span class="chat-img pull-left">
               <img src="http://placehold.it/50/55C1E7/fff" alt="User Avatar" class="img-circle">
              </span>
              <div class="chat-body clearfix">
                <div class="header">
                  <strong class="primary-font">Jack Sparrow</strong>
                  <small class="pull-right text-muted">
                  <i class="fa fa-clock-o fa-fw"></i> 12 mins ago
                  </small>
                </div>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales.
                </p>
              </div>
            </li>
            
            <li class="right clearfix">
              <span class="chat-img pull-right">
              <img src="http://placehold.it/50/FA6F57/fff" alt="User Avatar" class="img-circle">
              </span>
              <div class="chat-body clearfix">
                <div class="header">
                  <small class=" text-muted">
                   <i class="fa fa-clock-o fa-fw"></i> 13 mins ago</small>
                   <strong class="pull-right primary-font">Bhaumik Patel</strong>
                </div>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales.
                </p>
              </div>
            </li>
            
            <li class="left clearfix">
              <span class="chat-img pull-left">
               <img src="http://placehold.it/50/55C1E7/fff" alt="User Avatar" class="img-circle">
              </span>
              <div class="chat-body clearfix">
                <div class="header">
                 <strong class="primary-font">Jack Sparrow</strong>
                  <small class="pull-right text-muted">
                  <i class="fa fa-clock-o fa-fw"></i> 14 mins ago</small>
                </div>
                <p>
                   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales.
                </p>
              </div>
            </li>

            <li class="left clearfix">
              <span class="chat-img pull-left">
               <img src="http://placehold.it/50/55C1E7/fff" alt="User Avatar" class="img-circle">
              </span>
              <div class="chat-body clearfix">
                <div class="header">
                 <strong class="primary-font">Admin</strong>
                  <small class="pull-right text-muted">
                  <i class="fa fa-clock-o fa-fw"></i> 1 mins ago</small>
                </div>
                <p>
                   Jack Sparrow-:Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales.
                </p>
              </div>
            </li>
          </ul>
        </div>
        <!-- /.panel-body -->
        <div class="panel-footer">
          <div class="input-group">
            <input id="btn-input" type="text" class="form-control input-sm" placeholder="Type your message here...">
              <span class="input-group-btn">
               <button class="btn btn-warning btn-sm" id="btn-chat">Send</button>
              </span>
          </div>
        </div>
        <!-- /.panel-footer -->
    </div>
    </div>
</div>



  </div>
</div>

<footer class="container-fluid text-center danger-color-dark">
  <p>Footer Text</p>
</footer>
</div>

</body>
</html>

