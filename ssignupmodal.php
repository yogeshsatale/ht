
<!--Modal for sign up-->
     <!-- Modal content-->
<div class="modal fade" id="ssignupmodal" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
        
    <div class="modal-header danger-color-dark" style="padding:35px 50px;">
<button type="button" class="close font-effect-fire" onmouseover="" data-dismiss="modal">&times;</button>


         <h3><i class="fa fa-user"></i><span class=""> Registeration</span></h3>
    </div>
        
    <div class="modal-body" style="padding:40px 50px;">
        <form role="form" name="form" method="POST" id="contactform" action="#">
           
            <div class="md-form">
                <i class="fa fa-user prefix"></i>
                <input type="text" id="fname" name="fname" class="form-control " pattern="[a-zA-Z0-9\s]+"
                target="Please use alphabate only" required >
                <label for="fname"> Enter Full Name</label>
                <p id="p1"></p>
            </div>
            
            <div class="md-form">
                <i class="fa fa-user prefix"></i>
                <input type="text" id="fusername" name="username" class="form-control"required>
                <label for="username">Enter Username</label>
            </div>
            
            <div class="md-form">
                <i class="fa fa-lock prefix"></i>
                <input type="password" id="upassword" name="password" class="form-control" 
                minlength="6" title="Must contain at least 6 or more characters" required >
                <label for="password">Enter Password</label>
            </div>
            <div class="md-form">
                <i class="fa fa-lock prefix"></i><label for="cpassword"> Confirm Password</label>
                <input type="password" id="confirmpassword" name="cpassword" class="form-control" required 
                minlength="6" title="Must contain at least 6 or more characters">
            </div>
 
           
            
            <div class="md-form">
                <i class="fa fa-envelope prefix"></i>
                <input type="email" id="uemail" name="email"  class="form-control" required>
                <label for="email">Enter Email</label>
            </div>
            
            

             <div class="md-form">
                <i class="fa fa-home prefix" aria-hidden="true"></i>
                <input type="text" id="address" name="address" class="form-control" required>
                <label for="address">Enter Address</label>
            </div>

           <div class="md-form">
            <button type="submit" id="submit1"  class="btn btn-info btn-block" name="submit1" value="Send" >
               Sign up</button>
          </div>

        </form>
        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Close</button>
    </div>
<span id='message'></span>
   </div>
   </div>
   </div>  
  <script src="js/jquery.min.js"></script>
 <script type="text/javascript">

$(document).ready(function()
{
   


    //alert('data ready inserted successfully');
    $('#submit1').click(function()
    {
       // console.log("button clicked");
      // alert('data onclick inserted successfully');
       //var insert = [];
     //Get values of the input fields and store it into the variables.
        var submit1     = $("#submit1").val();
        var fname     = $("#fname").val();
        var username  = $("#fusername").val();
        var password  = $("#upassword").val();
        var cpassword = $("#confirmpassword").val();
        var email     = $("#uemail").val();
        var address   = $("#address").val();
     /*if(fname=='' || username=='' || password=='' || email=='' || address=='')
     {
        //alert('Please fill all requiered fields.');
        msgBoxImagePath = "images/";
               $.msgBox({
                  title: "Alert box",
                  content: "Please fill all requiered fields.",
                  type: "alert"
                });
     }
     else */
      //alert(username);
      if (password != cpassword) 
      {
           // alert(password);
            alert("Passwords do not match.");
            /*msgBoxImagePath = "images/";
               $.msgBox({
                  title: "Alert box",
                  content: "Passwords do not match.",
                  type: "alert"
                });*/
            return false;
      }
      
     else
     {
        
       //alert(fname);
        $.ajax
        ({
            url   : "ssignupinsert.php",
            method: "POST",
            data  : {submit1:submit1,fname:fname,username:username,password:password, email:email,
                address:address},
            success:function(data)
            {
                alert(data);
              if(data == 0) 
              {
                //alert(data);
                alert('Please fill all required fields');
                return false;
              }
              else if(data == 1)
              {
                   // alert(data);
                  alert('good job data inserted successfully');
              }
              else if(data == 2)
              {

                alert('User already exits');
                return false;
              }
            }
        });
     }
    return false; 
    });


return false; 
});

</script>