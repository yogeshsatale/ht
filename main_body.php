<!-- start: content -->
            <div id="content">
                
                <div class="panel">
                  <div class="panel-body">
                      <div class="col-md-6 col-sm-12">
                        <h3 class="animated fadeInLeft">HOMETutor Service</h3>
                        <p class="animated fadeInDown"><span class="fa  fa-map-marker"></span> Maharashtra,India</p>

                        <ul class="nav navbar-nav">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#" class="active">Feedback</a></li>
                            <li><a href="#">Student</a></li>
                            <li><a href="#">Teacher</a></li>
                            <li><a href="#">HR</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="col-md-6 col-sm-6 text-right" style="padding-left:10px;">
                          <h3 style="color:#DDDDDE;"><span class="fa  fa-map-marker"></span> Pune</h3>
                          <h1 style="margin-top: -10px;color: #ddd;">30<sup>o</sup></h1>
                        </div>
                        <div class="col-md-6 col-sm-6">
                           <div class="wheather">
                            <div class="stormy rainy animated pulse infinite">
                              <div class="shadow">
                                
                              </div>
                            </div>
                            <div class="sub-wheather">
                              
                              <div class="rain">
                                  <div class="droplet droplet1"></div>
                                  <div class="droplet droplet2"></div>
                                  <div class="droplet droplet3"></div>
                                  <div class="droplet droplet4"></div>
                                  <div class="droplet droplet5"></div>
                                  <div class="droplet droplet6"></div>
                              </div>
                            </div>
                          </div>
                        </div>                   
                    </div>
                  </div>                    
                </div>
     <!-- end of pannel -->
                <div class="col-md-12">
                    <div class="col-md-12 padding-0">
                        <div class="col-md-8 padding-0">
                            
                            <div class="col-md-12 padding-0">
                                <div class="col-md-6 padding-2 ">
                                  <div class="panel box-v1">
                                    <div class="panel-heading bg-white border-none">
                                      <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
                                          <h4 class="text-left">Visit</h4>
                                      </div>
                                      <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                        <h4>
                                        <span class="icon-user icons icon text-right"></span>
                                        </h4>
                                      </div>
                                    </div>
                                    <div class="panel-body text-center">
                                        <h1>51181,320</h1>
                                        <p>User active</p>
                                        <hr/>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel box-v1">
                                      <div class="panel-heading bg-white border-none">
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
                                          <h4 class="text-left">Enquiry</h4>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                           <h4>
                                           <span class="icon-basket-loaded icons icon text-right"></span>
                                           </h4>
                                        </div>
                                      </div>
                                 
                                      <div class="panel-body text-center">
                                      <a id="enquiries" href="#" style="curser:pointer;">
                                        <h1 id="enquiryno"></h1>
                                        <p>Enquiries Details</p></a>
                                        <hr/>
                                      </div>
                                    </div>
                                </div>
                            </div>

                          <!--Start Enquiry Table -->       
                            <div id="txtHint" class="col-md-12">
                              
                              
                                
                             
                              
                            </div>
                           <!--End of Enquiry Table -->    
    <!--End of col-md-8--></div>

                        <div class="col-md-4">
                            <div class="col-md-12 padding-0">
                              <div class="panel box-v2">
                                  <div class="panel-heading padding-0">
                                    <img src="asset/img/bg6.jpg" class="box-v2-cover img-responsive"/>
                                    <div class="box-v2-detail">
                                      <img src="asset/img/anshuman.jpg" class="img-responsive"/>
                                        
                                    </div>
                                  </div>
                                  <div class="panel-body">
                                    <div class="col-md-12 padding-0 text-center">
                                      <div class="col-md-4 col-sm-4 col-xs-6 padding-0">
                                          <h3>2.000</h3>
                                          <p>Post</p>
                                      </div>
                                      <div class="col-md-4 col-sm-4 col-xs-6 padding-0">
                                          <h3>2.232</h3>
                                          <p>share</p>
                                      </div>
                                      <div class="col-md-4 col-sm-4 col-xs-12 padding-0">
                                          <h3>4.320</h3>
                                          <p>photos</p>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                            </div>

                            <div class="col-md-12 padding-0">
                              <div class="panel box-v3"></div>
                            </div>

                            <div class="col-md-12 padding-0">
                              <div class="panel bg-light-blue">
                                <div class="panel-body text-white">
                                   <p class="animated fadeInUp quote">Lorem ipsum dolor sit amet, consectetuer adipiscing elit Ut wisi..."</p>
                                    <div class="col-md-12 padding-0">
                                      <div class="text-left col-md-7 col-xs-12 col-sm-7 padding-0">
                                        <span class="fa fa-twitter fa-2x"></span>
                                        <span>22 May, 2015 via mobile</span>
                                      </div>
                                      <div style="padding-top:8px;" class="text-right col-md-5 col-xs-12 col-sm-5 padding-0">
                                        <span class="fa fa-retweet"></span> 2000
                                        <span class="fa fa-star"></span> 3000
                                      </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="panel bg-green text-white">
                            <div class="panel-body">
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>

          <!-- end: content -->
</div>
     
