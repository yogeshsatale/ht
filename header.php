    <!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>HOMETutor</title>
  
   
<!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
 
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
   

<style type="text/css">

select{
    
    width:250px;

    padding:5px;
    border-radius:0px;
}

</style>




</head>

<body class="view">


    <!--Navigation & Intro-->
    <header>

        <!--Navbar-->
        <nav class="navbar navbar-dark navbar-fixed-top scrolling-navbar danger-color-dark">

            <!-- Collapse button-->
            <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#collapseEx">
            <i class="fa fa-bars"></i></button>

            <div class="container">

                <!--Collapse content-->
                <div class="collapse navbar-toggleable-xs" id="collapseEx">
                    <!--Navbar Brand-->
                    <a class="navbar-brand" href="" target="">HOMETutor</a>
                    <!--Links-->
                    <ul class="nav navbar-nav">
                        
                      <li class="nav-item">
                            <a class="nav-link" data-toggle="modal" data-target="#">About Us</a>
                        </li>
                     
                    <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#slmodal">Student Login</a>

                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="modal" data-target="#tlmodal">Teacher Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="modal" data-target="#adlmodal">Admin Login</a>
                        </li>
                    <li>
                        <button class="btn btn-primary pull-lg-right" type="submit" data-toggle="modal" data-target="#search" href="#!">Search Teacher</button>   
                    </li>
                    
                    </ul>
                   
                    

                    

                   

                </div>
                <!--/.Collapse content-->

            </div>

        </nav>
        <!--/.Navbar-->

    </header>
    <!--/Navigation & Intro-->
