<?php
include 'aheader.php';
include 'asidebar.php';
?>
      <!-- start: Content -->
      		<div id="content">
            <div class="col-md-12" style="padding:20px;">
              <div class="col-md-12 mail-wrapper">
                    
                  <div class="col-md-12 padding-0">
                      <div class="col-md-3 mail-left">
                          <div class="col-md-12 mail-left-header">
                                <center>
                                <input type="button" class="btn btn-danger btncompose-mail" value="Compose Mail"/>
                                </center>
                          </div>
                          <div class="col-md-12 mail-left-content">
                               <ul class="nav">
                                  <li></li>
                                  <li><h5>Folder</h5></li>
                                  <li>
                                    <a href="" class="active"><span class="fa fa-inbox"></span> Inbox (20)</a>
                                  </li>
                                  <li>
                                    <a href=""><span class="fa fa-send-o"></span> Send Mail</a>
                                  </li>
                                  <li>
                                    <a href=""><span class="fa fa-folder"></span> Drafts</a>
                                  </li>
                                  <li>
                                    <a href=""><span class="fa fa-lock"></span> Important</a>
                                  </li>
                                  <li>
                                    <a href=""><span class="fa fa-star"></span> Favorite</a>
                                  </li>
                                  <li>
                                    <a href=""><span class="fa fa-fire"></span> Spam</a>
                                  </li>
                                  <li>
                                    <a href=""><span class="fa fa-trash"></span> Trash</a>
                                  </li>
                                  <li><hr/></li>
                                  <li><h5>Categories</h5></li>
                                  <li>
                                    <a href=""><div class="fa fa-circle text-primary"></div> Social</a>
                                  </li>
                                   <li>
                                    <a href=""><div class="fa fa-circle text-success"></div> Advertising</a>
                                  </li>
                                   <li>
                                    <a href=""><div class="fa fa-circle text-info"></div> Forum</a>
                                  </li>
                                   <li>
                                    <a href=""><div class="fa fa-circle text-warning"></div> News</a>
                                  </li>
                                  <li>
                                    <a href=""><span class="fa fa-circle text-danger"></span> Document</a>
                                  </li>
                                  <li><hr/></li>
                                  <li><h5>Tags</h5></li>
                                  <li>
                                    <ul class="tags">
                                        <li><a href=""><span class="fa fa-tag"></span> Hacking</a></li>
                                        <li><a href=""><span class="fa fa-tag"></span> Phising</a></li>
                                        <li><a href=""><span class="fa fa-tag"></span> Cracking</a></li>
                                        <li><a href=""><span class="fa fa-tag"></span> CSRF</a></li>
                                        <li><a href=""><span class="fa fa-tag"></span> XSS</a></li>
                                    </ul>
                                  </li>
                              </ul>
                          </div>
                      </div>
                      <div class="col-md-9 mail-right">
                          <div class="col-md-12 mail-right-header">
                            <div class="col-md-10 col-sm-10 padding-0">
                                 <div class="input-group searchbox-v1">
                                  <span class="input-group-addon  border-none box-shadow-none" id="basic-addon1">
                                    <span class="fa fa-search"></span>
                                  </span>
                                  <input type="text" class="txtsearch border-none box-shadow-none" placeholder="Search Email..." aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 padding-0 text-right mail-right-options">
                                 <div class="btn-group pull-right right-option-v1">
                                    <i class="fa fa-ellipsis-v right-option-v1-icon" data-toggle="dropdown"></i>
                                    <ul class="dropdown-menu" role="menu">
                                      <li><a href="#">Action</a></li>
                                      <li><a href="#">Another action</a></li>
                                      <li><a href="#">Something else here</a></li>
                                      <li class="divider"></li>
                                      <li><a href="#">Separated link</a></li>
                                    </ul>
                                  </div>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 mail-right-tool">
                              <ul class="nav">
                                  <li>
                                    <input type="checkbox" class="icheck" name="checkbox1" />
                                  </li>
                                  <li>
                                    <a href=""><span class="fa fa-eye"></span></a>
                                  </li>
                                  <li>
                                    <a href=""><span class="fa fa-star"></span></a>
                                  </li>
                                  <li>
                                    <a href=""><span class="fa fa-lock"></span></a>
                                  </li>
                                  <li>
                                    <a href=""><span class="fa fa-folder"></span></a>
                                  </li>
                                  <li>
                                    <a href=""><span class="fa fa-fire"></span></a>
                                  </li>
                                  <li>
                                    <a href=""><span class="fa fa-trash"></span></a>
                                  </li>
                                  <li class="nav navbar-right" >
                                      <ul class="nav">
                                        <li> <a href=""><span class="fa fa-angle-left"></span></a></li>
                                        <li> <a href="" class="btn-info"><span class="fa fa-angle-right"></span></a></li>
                                      </ul>
                                  </li>
                              </ul>
                          </div>
                          <div class="col-md-12 mail-right-content">
                              <table class="table table-hover">
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact">
                                      <a href="">Google  </a> <span class="label pull-right label-danger">Document</span>
                                    </td>
                                     <td class="subject">
                                      <a href="#">Nunc nonummy metus. Mauris sollicitudin fermentum libero.</a>
                                      </td>
                                  </tr>
                                  <tr class="unread">
                                    <td class="check"><input type="checkbox" checked="checked" class="icheck" name="checkbox1" /></td>
                                    <td class="contact">Facebook</td>
                                    <td class="subject">
                                      <a href="#">Quisque id odio. Maecenas nec odio et ante tincidunt tempus.</a>
                                    </td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact">
                                      <a href=""#>Priscilla Burke </a> <span class="label pull-right label-primary"> Social</span>
                                    </td>
                                     <td class="subject">
                                        <a href="#">Curabitur at lacus ac velit ornare lobortis. </a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact">
                                      <a>Priscilla Burke </a> <span class="label pull-right label-success">Ads</span></td>
                                     <td class="subject"><a href="#">Sed in libero ut nibh placerat accumsan. Vivamus consectetuer hendrerit lacus.</a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact"><a href=""#>Priscilla Burke</a></td>
                                     <td class="subject"><a href="#">Nam ipsum risus, rutrum vitae, vestibulum eu, volutpat a, suscipit non, turpis.</a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact"><a href=""#>Priscilla Burke</a></td>
                                     <td class="subject"><a href="#">Quisque libero metus, condimentum nec, tempor a,Morbi ac felis.</a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact"><a href=""#>Priscilla Burke</a></td>
                                     <td class="subject"><a href="#">Praesent venenatis metus at tortor pulvinar varius. Proin veros.</a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact"><a href=""#>Priscilla Burke</a></td>
                                     <td class="subject"><a href="#">Praesent ac sem eget est egestas volutpat. Quisque id odio.</a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact"><a href=""#>Priscilla Burke</a></td>
                                     <td class="subject"><a href="#">Nullam quis ante. In ut quam vitae odio lacinia tincidunt.</a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact"><a href=""#>Priscilla Burke</a></td>
                                     <td class="subject"><a href="#">Curabitur nisi. Sed in libero ut nibh placerat accumsan.</a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact"><a href=""#>Priscilla Burke</a></td>
                                     <td class="subject"><a href="#">Curabitur nisi. Suspendisse potenti.</a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact"><a href=""#>Priscilla Burke</a></td>
                                     <td class="subject"><a href="#">Duis leo. Nam pretium turpis et arcu.</a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact"><a href=""#>Priscilla Burke</a></td>
                                     <td class="subject"><a href="#">Ut tincidunt tincidunt erat. Phasellus magna.</a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact"><a href=""#>Priscilla Burke</a></td>
                                     <td class="subject"><a href="#">Maecenas ullamcorper, dui et placerat feugiat</a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact"><a href=""#>Priscilla Burke</a></td>
                                     <td class="subject"><a href="#">Quisque ut nisi. Nulla facilisi.</a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact">
                                      <a>Priscilla Burke </a> <span class="label pull-right label-info">Forum</span>
                                    </td>
                                     <td class="subject"><a href="#">Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula</a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact"><a href=""#>Priscilla Burke</a></td>
                                     <td class="subject"><a href="#">Suspendisse feugiat. Curabitur blandit mollis lacus.</a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact">
                                      <a href="#">Priscilla Burke </a> <span class="label pull-right label-warning">News</span>
                                    </td>
                                     <td class="subject"><a href="#">Duis lobortis massa imperdiet quam. Proin faucibus arcu </a></td>
                                  </tr>
                                  <tr class="read">
                                    <td class="check"><input type="checkbox" class="icheck" name="checkbox1" /></td>
                                    <td class="contact"><a href=""#>Priscilla Burke</a></td>
                                     <td class="subject"><a href="#">Morbi nec metus. Pellentesque libero to semper nec, quam.</a></td>
                                  </tr>
                              </table>
                          </div>
                      </div>
                  </div>

              </div>
            </div>
      		</div>
          <!-- end: Content -->
</div>      
<?php
include 'afooter.php';
?>      
   
    
   

    